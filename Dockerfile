FROM openjdk:8-jre-alpine
COPY ./target/myapp-0.1.0.jar /myapp.jar
CMD ["java", "-jar", "/myapp.jar"]

